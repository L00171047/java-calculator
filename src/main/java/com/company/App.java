package com.company;

import java.util.Scanner;

/**
 * Works like a calculator PUSHING CODE
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("****** Welcome to Calculator ******");
        System.out.println("Operation List ---->\n 1.Addition \n 2.Subtraction \n 3.Multiplication");
        System.out.print("Please enter your operation Number : ");
        Scanner scanner = new Scanner(System.in);
        int operation = scanner.nextInt();
        System.out.print("Please enter Number 1 : ");
        int a = scanner.nextInt();
        System.out.print("Please enter Number 2 : ");
        int b = scanner.nextInt();
        if (operation==1){
            System.out.println("The Result is : "+addTwoNumbers(a,b));
        }else if(operation==2){
            System.out.println("The Result is : "+subTwoNumbers(a,b));
        }else if(operation==3){
            System.out.println("The Result is : "+multiplyTwoNumbers(a,b));
        }else{
            System.out.println("Please enter a valid operation number");
        }
        scanner.close();

    }

    public static int addTwoNumbers(int a, int b) {
        return a + b;
    }

    public static int subTwoNumbers(int a, int b) {
        return a - b;
    }

    public static int multiplyTwoNumbers(int a, int b) {
        return a*b;
    }

}
